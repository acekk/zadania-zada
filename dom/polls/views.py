# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.contrib import messages

from models import Poll, Choice


def vote_view(request, pk):
    poll = get_object_or_404(Poll, pk=pk)
    if request.method == "POST":
        try:
            # sprawdzenie wyboru
            # walidacja po stronie serwera jest zawsze konieczna!
            choice = poll.choice_set.get(
                        pk=request.POST.get('choice', 0))
        except Choice.DoesNotExist: # błąd — odesłanie widoku szczegółowego
            msg = u"Ups, wybierz odpowiedź, która istnieje"
            messages.add_message(request, messages.ERROR, msg)
            url = reverse('poll_detail', args=[pk, ])
        else: # zapisanie głosu i odesłanie wyników
            choice.votes += 1
            choice.save()
            messages.add_message(request, messages.INFO,
                                 u"Zagłosowałeś na %s" % choice)
            url = reverse('poll_result', args=[pk])
    else: # wysłane metodą GET — ignorujemy
        url = reverse('poll_detail', args=[pk, ])
    # przekierowanie
    return HttpResponseRedirect(redirect_to=url)

